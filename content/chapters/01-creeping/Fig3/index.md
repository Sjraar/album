---
title: "Fig 3. Hele-Shaw flow past an inclined plate"
date: 2020-08-12
weight: 3
draft: true
featured: true
tags: ["Finite element", "FEM", "Nutils"]
authors:
  - "steinstoter"
---

The Hele-Shaw analogy cannot represent a flow with circulation. It therefore shows the streamlines of potential flow past an inclined plate with zero ligt. Dye flows in water between glass plates spaced 1mm apart. *Photograph by D. H. Peregrine*