---
title: "Fig 2. Hele-Shaw flow past a Rankine half-body"
date: 2020-08-11
draft: true
weight: 2
featured: true
tags: ["Finite volume", "Ansys"]
authors:
  - "steinstoter"
---

A viscous fluid is introduced through the orifice at the left into a uniform stream of the same fluid flowing between glass plates spaced 0.5 mm apart. Dye shows both the external and internal streamlines for plane potential flow past a semi-infinite body. The streamlines are slightly blurred because the rate of delivery of fluid to the source was changing as the photograph was taken. *Photograph by D. H. Peregrine*