---
title: "Fig 45. Circular cylinder at R=28.4"
date: 2023-08-18
weight: 45
featured: false
tags: ["StarCCM", "FVM", "Laminar", "Flow past cylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "45full.png" "Experiment" "Simulation">}}
*"Here just the boundary of the recirculating region has been made visible by coating the cylinder with condensed milk and setting it in motion through water."* Photograph by Sadathoshi Taneda

## General Info
This post is part of a series on flow separation, studied for the case of flow past a circular cylinder at different Reynolds numbers. In the current figure, it is clearly visible that the vortices behind the cylinder, which appear due to separation, grew in comparison to earlier figures in this series. Also, one can see that the flow separates at an earlier point on the cylinder. 

The main theory and simulation and visualization set-up are discussed in the [web post from Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}). The full series is:
- [Figure 24]({{< ref "/chapters/02-Laminar/Fig24" >}}): Circular cylinder at R=1.54.
- [Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}): Circular cylinder at R=9.6.
- [Figure 41]({{< ref "/chapters/03-Separation/Fig41" >}}): Circular cylinder at R=13.1.
- [Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}): Circular cylinder at R=26.
- [Figure 45]({{< ref "/chapters/03-Separation/Fig45" >}}): Circular cylinder at R=28.4.
- [Figure 46]({{< ref "/chapters/03-Separation/Fig46" >}}): Circular cylinder at R=41.
- [Figure 96]({{< ref "/chapters/04-Vortices/Fig96" >}}): Kármán vortex street behind a circular cylinder at R=105.
- [Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}): Kármán vortex street behind a circular cylinder at R=140.

An overview of these posts can be viewed here:

{{< youtube lGce673o8mA >}}